# Generated by Django 4.1.7 on 2023-03-03 18:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("receipts", "0002_alter_receipt_category"),
    ]

    operations = [
        migrations.AlterField(
            model_name="receipt",
            name="category",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="receipts",
                to="receipts.expensecategory",
            ),
        ),
    ]
